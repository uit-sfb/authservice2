package no.uit.metapipe.auth.core;

import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class TokenTest {
    @Test
    public void token_secret_validation() {
        Token token = new Token();
        String tokenSecret = token.generateTokenSecret();
        assertThat(token.validateTokenSecret(tokenSecret)).isEqualTo(true);
        assertThat(token.validateTokenSecret("non-valid-secret")).isEqualTo(false);

        assertThat(tokenSecret.contains("/")).isEqualTo(false);
    }
}
