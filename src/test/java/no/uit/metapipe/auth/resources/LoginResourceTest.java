package no.uit.metapipe.auth.resources;

import fixtures.FixtureData;
import io.dropwizard.testing.junit.ResourceTestRule;
import no.uit.metapipe.auth.config.AuthenticationCookieConfig;
import no.uit.metapipe.auth.api.AccessToken;
import no.uit.metapipe.auth.api.UserDto;
import no.uit.metapipe.auth.service.TokenService;
import no.uit.metapipe.auth.service.UserService;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;

public class LoginResourceTest {
    private static final TokenService TOKEN_SERVICE = mock(TokenService.class);
    private static final UserService userService = mock(UserService.class);

    private static final String validUsername = "validUsername";
    private static final String nonValidUsername = "nonValidUsername";
    private static final String nonValidPassword = "nonValidPassword";
    private static final String validPassword = "validPassword";
    private static final String redirectSuccessUrl = "http://example.com/success";
    private static final String redirectFailureUrl = "http://example.com/failure";
    private static final String validAccessToken = new AccessToken("valid-token-id", "valid-token-secret").getToken();

    private static final AuthenticationCookieConfig cookieConf = AuthenticationCookieConfig.builder()
            .cookieName("test-cookie-name")
            .build();

    private static final Optional<String> noAccessToken = Optional.empty();

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new LoginResource(cookieConf, userService, TOKEN_SERVICE))
            .build();

    @Before
    public void setup() {
        reset(TOKEN_SERVICE);

        FixtureData.getDefault().apply(resources.client(), "");

        UserDto validUserDto = new UserDto();
        validUserDto.setUsername(validUsername);

        when(userService.registerUsernamePassword(validUsername, validPassword))
                .thenReturn(validUserDto);

        when(TOKEN_SERVICE.authenticateUsernamePassword(validUsername, validPassword))
                .thenReturn(Optional.of(validAccessToken));
        when(TOKEN_SERVICE.authenticateUsernamePassword(nonValidUsername, nonValidPassword))
                .thenReturn(noAccessToken);
    }

    private Form loginForm(String username, String password) {
        Form form = new Form();
        form.param("username", username);
        form.param("password", password);
        form.param("redirectSuccess", redirectSuccessUrl);
        form.param("redirectFailure", redirectFailureUrl);
        return form;
    }

    private Form userRegistrationForm(String username, String password, String passwordRepeat) {
        Form form = new Form();
        form.param("username", username);
        form.param("password", password);
        form.param("passwordRepeat", passwordRepeat);
        form.param("redirectSuccess", redirectSuccessUrl);
        form.param("redirectFailure", redirectFailureUrl);
        return form;
    }

    private Response registerUser(String username, String password, String passwordRepeat) {
        Client client = resources.client();
        client.property(ClientProperties.FOLLOW_REDIRECTS, "false");

        return client.target("/login/register").request()
                .post(Entity.form(userRegistrationForm(username, password, passwordRepeat)));
    }

    public void assertValidRedirect(Response response, String redirectTo) {
        assertThat(response.getStatus()).isEqualTo(Response.Status.SEE_OTHER.getStatusCode());
        assertThat(response.getHeaderString("Location")).isEqualTo(redirectTo);
    }

    public void assertValidAuthCookie(Response response) {
        assertThat(response.getCookies().get(cookieConf.getCookieName()).toCookie().getValue()).isEqualTo(validAccessToken);
    }

    @Test
    public void successful_authentication() {
        Client client = resources.client();
        client.property(ClientProperties.FOLLOW_REDIRECTS, "false");

        Response response = client.target("/login").request()
                .post(Entity.form(loginForm(validUsername, validPassword)));

        assertThat(response.getStatus()).isEqualTo(Response.Status.SEE_OTHER.getStatusCode());
        assertThat(response.getHeaderString("Location")).isEqualTo(redirectSuccessUrl);
        assertThat(response.getCookies().get(cookieConf.getCookieName()).toCookie().getValue()).isEqualTo(validAccessToken);
    }

    @Test
    public void unsuccessful_authentication() {
        Client client = resources.client();
        client.property(ClientProperties.FOLLOW_REDIRECTS, "false");

        Response response = client.target("/login").request()
                .post(Entity.form(loginForm(nonValidUsername, nonValidPassword)));

        assertThat(response.getStatus()).isEqualTo(Response.Status.SEE_OTHER.getStatusCode());
        assertThat(response.getHeaderString("Location")).isEqualTo(redirectFailureUrl);
    }

    @Test
    public void successful_user_registration() {
        Response response = registerUser(validUsername, validPassword, validPassword);
        verify(userService).registerUsernamePassword(validUsername, validPassword);
        assertValidRedirect(response, redirectSuccessUrl);
        assertValidAuthCookie(response);
    }

    @Test
    public void unsuccessful_non_equal_passwords() {
        Response response = registerUser(validUsername, validPassword, nonValidPassword);
        verify(userService, never()).registerUsernamePassword(anyString(), anyString());
        assertValidRedirect(response, redirectFailureUrl);
    }
}
