package no.uit.metapipe.auth.utils;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.*;

class UriScopeTestHelper {
    public UriScope parent;

    public static UriScopeTestHelper fromParentString(String parent) {
        return new UriScopeTestHelper(UriScope.fromString(parent));
    }

    public UriScopeTestHelper(UriScope parent) {
        this.parent = parent;
    }

    public void assertAuthorized(String child) {
        assertThat(UriScope.fromString(child).isAuthorizedBy(parent)).isTrue();
    }

    public void assertNotAuthorized(String child) {
        assertThat(UriScope.fromString(child).isAuthorizedBy(parent)).isFalse();
    }

    public void assertAuthorized(String method, String uri) {
        assertThat(parent.authorizesMethodAndUri(method, uri)).isTrue();
    }

    public void assertNotAuthorized(String method, String uri) {
        assertThat(parent.authorizesMethodAndUri(method, uri)).isFalse();
    }
}

public class UriScopeTest {
    public static final String validSerializedScope = "GET,POST|hello/world";

    public UriScope s(String str) {
        return UriScope.fromString(str);
    }

    @Test
    public void scope_should_serialize_correctly() {
        UriScope scope = UriScope.fromString("GET,POST|hello/world");
        assertThat(scope.encodeString()).contains("GET");
        assertThat(scope.encodeString()).contains("POST");
        assertThat(scope.encodeString()).contains("|hello/world");
    }

    @Test
    public void scope_should_deserialize_correctly() {
        UriScope scope = UriScope.fromString(validSerializedScope);
        assertThat(scope.getMethods().contains("GET")).isTrue();
        assertThat(scope.getMethods().contains("POST")).isTrue();
        assertThat(scope.getUri()).isEqualTo("hello/world");
    }

    @Test
    public void scope_comparison() {
        UriScopeTestHelper storageTest = UriScopeTestHelper.fromParentString("GET,PUT|storage/test");
        // Authorized
        storageTest.assertAuthorized("GET|storage/test/hello");
        storageTest.assertAuthorized("GET,PUT|storage/test/hello");
        storageTest.assertAuthorized("GET,PUT|storage/test");
        // Not
        storageTest.assertNotAuthorized("POST|storage/test/hello");
        storageTest.assertNotAuthorized("GET|storage/");
        storageTest.assertNotAuthorized("GET|storage/hello/world");


        UriScopeTestHelper storage = UriScopeTestHelper.fromParentString("GET,PUT|storage");
        // Authorized
        storage.assertAuthorized("GET|storage/test/hello");
        // Not
        storage.assertNotAuthorized("GET|jobs");


        UriScopeTestHelper storageSlash = UriScopeTestHelper.fromParentString("GET,PUT|storage/");
        // Authorized
        storageSlash.assertAuthorized("GET|storage/test");
        // Not
        storageSlash.assertNotAuthorized("GET|storage");
    }

    @Test
    public void wildcards() {
        UriScopeTestHelper executorStorage = UriScopeTestHelper.fromParentString("GET,PUT|storage/*/stallo");
        executorStorage.assertNotAuthorized("GET|storage/");
        executorStorage.assertNotAuthorized("GET|storage/test/ice2");

        executorStorage.assertAuthorized("GET|storage/test/stallo/inputs");
        executorStorage.assertAuthorized("GET,PUT|storage/*/stallo/inputs");
    }

    @Test
    public void scope_allows_methods_and_uri() {
        UriScopeTestHelper storageTest = UriScopeTestHelper.fromParentString("GET,PUT|storage/test/");
        storageTest.assertAuthorized("GET", "storage/test/hello");

        storageTest.assertNotAuthorized("GET", "storage/test");
        storageTest.assertNotAuthorized("PUT", "storage/test");
        storageTest.assertNotAuthorized("POST", "storage/test");
        storageTest.assertNotAuthorized("GET", "storage/");
    }

    @Test
    public void hashCodeShouldWork() {
        assertThat(s("GET,POST|hello").hashCode()).isEqualTo(s("GET,POST|hello").hashCode());
    }

    @Test
    public void equals_should_work() {
        assertThat(UriScope.fromString("GET,POST|/hello/world")).isEqualTo(UriScope.fromString("GET,POST|/hello/world"));
        assertThat(UriScope.fromString("GET,POST|/hello/world")).isNotEqualTo(UriScope.fromString("GET|/hello/world"));

        assertThat(s("GET,POST|hello")).isEqualTo(s("GET,POST|hello"));
        assertThat(s("PUT,GET|yes/please")).isEqualTo(s("PUT,GET|yes/please"));
    }

    @Test
    public void it_should_be_possible_to_confine_a_scope() {
        assertThat(s("GET,PUT|test/hello").confinedTo(s("GET,POST|test/hello/world"))).isEqualTo(s("GET|test/hello/world"));
    }

    @Test
    public void remove_redundant_scopes() {
        Set<UriScope> scopes = new HashSet<>();
        scopes.add(s("GET,POST|hello"));
        scopes.add(s("GET,POST|hello"));
        scopes.add(s("GET|hello/world"));
        scopes.add(s("GET|hello/there"));
        scopes.add(s("GET|hello"));
        scopes.add(s("PUT|yes/please"));
        scopes.add(s("PUT,GET|yes/please"));

        Set<UriScope> nonRedundant = new HashSet<>();
        for (UriScope s : UriScope.removeRedundantScopes(scopes)) {
            nonRedundant.add(s);
        }

        Set<UriScope> expected = new HashSet<>();
        expected.add(s("GET,POST|hello"));
        expected.add(s("PUT,GET|yes/please"));

        assertThat(nonRedundant).isEqualTo(expected);
    }

    @Test
    public void scope_confinement() {
        // Request is more specific
        UriScope authorized = s("GET,PUT|storage/testuser");
        UriScope requested = s("GET|storage/testuser/my/file");
        assertThat(authorized.canBeConfinedTo(requested)).isTrue();
        assertThat(authorized.confinedTo(requested)).isEqualTo(requested);

        authorized = s("GET|storage/testuser/ex");
        requested = s("GET,PUT|storage/testuser/ex/hello");
        assertThat(authorized.canBeConfinedTo(requested)).isTrue();
        assertThat(authorized.confinedTo(requested)).isEqualTo(s("GET|storage/testuser/ex/hello"));
        requested = s("GET|storage/testuser");
        assertThat(authorized.canBeConfinedTo(requested)).isFalse();

        // Request is more general
        requested = s("GET,PUT,POST|storage/");
        assertThat(authorized.canBeConfinedTo(requested)).isFalse();

        // Allowed scope contains a wildcard
        authorized = s("GET,PUT|storage/*/executors/stallo");
        requested = s("GET|storage/testuser/executors/stallo");
        assertThat(authorized.canBeConfinedTo(requested)).isTrue();
        assertThat(authorized.confinedTo(requested)).isEqualTo(requested);
    }

    @Test
    public void confine_multiple_scopes() {
        Set<UriScope> authorizedScopes = new HashSet<>();
        authorizedScopes.add(s("GET|storage/testuser/ex"));

        Set<UriScope> requestedScopes = new HashSet<>();
        requestedScopes.add(s("GET,PUT|storage/testuser/ex/hello"));
        requestedScopes.add(s("GET|storage/testuser"));

        Set<UriScope> expected = new HashSet<>();
        expected.add(s("GET|storage/testuser/ex/hello"));

        Set<UriScope> actual = UriScope.confineRequestedScopes(authorizedScopes, requestedScopes);
        assertThat(actual).isEqualTo(expected);
    }
}
