package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.config.OAuthConfig;
import no.uit.metapipe.auth.api.AccessToken;
import no.uit.metapipe.auth.core.Token;
import no.uit.metapipe.auth.db.TokenRepo;
import no.uit.metapipe.auth.utils.TokenFactory;
import java.time.Duration;
import org.junit.Test;

import java.time.Instant;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TokenExpirationTest {

    @Test
    public void expire_token() {

        OAuthConfig oAuthConfig = new OAuthConfig();
        TokenFactory tokenFactory = new TokenFactory();
        Token token = tokenFactory.createToken();
        token.setActive(true);
        String tokenSecret = token.generateTokenSecret();
        String accessToken = new AccessToken(token.getTokenId(), tokenSecret).getToken();

        // set expiration
        token.setExpiration(Instant.now().toEpochMilli() + oAuthConfig.getAccessTokenExpirationMillis());

        TokenRepo tokenRepo = mock(TokenRepo.class);
        when(tokenRepo.findByTokenId(token.getTokenId())).thenReturn(Optional.of(token));

        TokenServiceImpl tokenService = new TokenServiceImpl(null, tokenRepo, null, null, null);

        // check token not expired (+ token secret + token is active)
        assertThat(tokenService.getByTokenString(accessToken)).isEqualTo(Optional.of(token));

        // expire token
        token.setExpiration(Instant.now().minus(Duration.ofMillis(2)).toEpochMilli());

        // check expired
        assertThat(tokenService.getByTokenString(accessToken)).isEqualTo(Optional.empty());
    }
}
