package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.config.OAuthConfig;
import no.uit.metapipe.auth.core.PasswordDetails;
import no.uit.metapipe.auth.core.Scope;
import no.uit.metapipe.auth.core.Token;
import no.uit.metapipe.auth.core.User;
import no.uit.metapipe.auth.db.ScopeRepo;
import no.uit.metapipe.auth.db.TokenRepo;
import no.uit.metapipe.auth.db.UserRepo;
import no.uit.metapipe.auth.utils.TokenFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TokenServiceTest {
    private static final String validUsername = "validUsername";
    private static final String validPassword = "validPassword123";
    private static final String validIssuer = "validIssuer";
    private static final String validTokenId = "validTokenId";
    private static final String validTokenSecret = "validTokenSecret";
    private static final String validAccessToken = validTokenId + "." + validTokenSecret;

    private static final UserRepo userRepo = mock(UserRepo.class);
    private static final TokenRepo TOKEN_REPO = mock(TokenRepo.class);
    private static final ScopeRepo scopeRepo = mock(ScopeRepo.class);
    private static final OAuthConfig oAuthConfig = mock(OAuthConfig.class);

    private static final TokenFactory TOKEN_FACTORY = mock(TokenFactory.class);
    private static final TokenService TOKEN_SERVICE = new TokenServiceImpl(userRepo, TOKEN_REPO, scopeRepo, TOKEN_FACTORY, oAuthConfig);

    @Before
    public void setup() {
        reset(userRepo);

        User user = new User();
        user.setUsername(validUsername);
        PasswordDetails passwordDetails = mock(PasswordDetails.class);
        when(passwordDetails.isValidPassword(validPassword))
                .thenReturn(true);

        user.setPasswordDetails(passwordDetails);
        when(userRepo.findByUsername(validUsername)).thenReturn(user);

        Token token = new Token();
        token.setTokenId(validTokenId);
        token.setSubject(user);
        when(TOKEN_FACTORY.createToken()).thenReturn(token);

        when(scopeRepo.saveOrUpdate(any(Scope.class))).then(AdditionalAnswers.returnsFirstArg());
    }

    @Test
    public void successful_authentication_should_yield_a_valid_token() {
        Optional<String> accessTokenOptional = TOKEN_SERVICE.authenticateUsernamePassword(validUsername, validPassword);
        assertThat(accessTokenOptional.isPresent()).isTrue();

        String accessToken = accessTokenOptional.get();
        assertThat(accessToken.split("\\.").length).isEqualTo(2);
    }
}
