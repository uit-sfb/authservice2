package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.core.Scope;
import no.uit.metapipe.auth.db.AuthorizationCodeRepo;
import no.uit.metapipe.auth.db.ScopeRepo;
import no.uit.metapipe.auth.db.TokenRepo;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import static org.mockito.Mockito.*;

import static org.assertj.core.api.Assertions.assertThat;

public class AuthorizationServiceTest {
    private final ScopeRepo scopeRepo = mock(ScopeRepo.class);
    private final TokenRepo tokenRepo = mock(TokenRepo.class);
    private final TokenService tokenService = mock(TokenService.class);

    private final AuthorizationCodeRepo authorizationCodeRepo = mock(AuthorizationCodeRepo.class);

    private final AuthorizationServiceImpl authorizationService = new AuthorizationServiceImpl(scopeRepo, tokenRepo, tokenService, authorizationCodeRepo);
}
