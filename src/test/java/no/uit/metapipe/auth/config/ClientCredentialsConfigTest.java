package no.uit.metapipe.auth.config;

import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class ClientCredentialsConfigTest {
    @Test
    public void correct_secret_should_validate() {
        ClientCredentialsConfig config = ClientCredentialsConfig.createFromClientId("test", 10);
        assertThat(config.compareSecret(config.getGeneratedSecret())).isTrue();
    }

    @Test
    public void incorrect_secret_should_not_validate() {
        ClientCredentialsConfig config = ClientCredentialsConfig.createFromClientId("test", 10);
        assertThat(config.compareSecret("not-likely-generated")).isFalse();
    }
}
