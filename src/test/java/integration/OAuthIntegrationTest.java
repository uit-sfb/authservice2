package integration;

import fixtures.FixtureData;
import no.uit.metapipe.auth.api.IntrospectionResponse;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

public class OAuthIntegrationTest {
    @ClassRule
    public static IntegrationTestFixture rule = new IntegrationTestFixture();

    private Client client;

    private String serverLocation;


    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
        this.serverLocation = rule.getServerLocation();

        FixtureData fixtureData = FixtureData.getDefault();
        fixtureData.apply(client, this.serverLocation);
    }

    @After
    public void tearDown() {
        this.client.close();
    }

    @Test
    public void password_grant_should_work() throws Exception {
        OAuthAccessTokenResponse response = rule.requestAccessToken("validUsername", "validPassword123", "GET,PUT|storage/NOT_validUsername GET,PUT|storage/validUsername");
        assertThat(response.getAccessToken()).isNotNull().isNotEmpty();
        assertThat(response.getScope()).contains("GET,PUT|storage/validUsername");
        assertThat(response.getScope()).doesNotContain("NOT_validUsername");

        IntrospectionResponse introspectionResponse = rule.introspect(response.getAccessToken());
        assertThat(introspectionResponse.isActive()).isTrue();
        assertThat(introspectionResponse.getScope()).contains("GET,PUT|storage/validUsername");
        assertThat(introspectionResponse.getScope()).doesNotContain("NOT_validUsername");
    }

    @Test
    public void invalid_password_should_not_work() throws Exception {
        try {
            OAuthAccessTokenResponse response = rule.requestAccessToken("validUsername", "INVALID_password", "");
        } catch (OAuthProblemException e) {
            assertThat(e.getError()).isEqualTo("invalid_grant");
            return;
        }
        fail("invalid password did not throw the correct exception");
    }

    @Test
    public void token_revocation_should_work() throws Exception {
        OAuthAccessTokenResponse response = rule.requestAccessToken();
        assertThat(response.getAccessToken()).isNotNull().isNotEmpty();

        // Ensure that we have a valid access token
        IntrospectionResponse introspectionResponse = rule.introspect(response.getAccessToken());
        assertThat(introspectionResponse.isActive()).isTrue();

       //stuff here
        Form form = new Form();
        form.param("token", response.getAccessToken());
        Response rev_response = client.target(serverLocation + "/oauth2/revoke")
                .request()
                .post(Entity.form(form));
        assertThat(rev_response.getStatus()).isEqualTo(200);

        // Ensure that the access token is no longer valid
        introspectionResponse = rule.introspect(response.getAccessToken());
        assertThat(introspectionResponse.isActive()).isFalse();
    }

    @Test
    public void refreshing_an_access_token_should_work() throws Exception {
        OAuthAccessTokenResponse response = rule.requestAccessToken();
        String firstAccessToken = response.getAccessToken();
        String refreshToken = response.getRefreshToken();

        assertThat(firstAccessToken).isNotNull().isNotEmpty();
        assertThat(refreshToken).isNotNull().isNotEmpty();

        String limitedScope = "GET|storage/validUsername/someUniqueFile";

        OAuthClientRequest request = rule.tokenRequestBuilder()
                .setClientId(rule.getWebClientId())
                .setClientSecret(rule.getWebClientSecret())
                .setGrantType(GrantType.REFRESH_TOKEN)
                .setRefreshToken(refreshToken)
                .setScope(limitedScope)
                .buildQueryMessage();

        response = rule.getOauthClient().accessToken(request);
        String secondAccessToken = response.getAccessToken();

        assertThat(secondAccessToken).isNotNull().isNotEmpty();
        assertThat(secondAccessToken).isNotEqualTo(firstAccessToken);
        assertThat(response.getScope()).contains(limitedScope);

        IntrospectionResponse introspectionResponse = rule.introspect(response.getAccessToken());
        assertThat(introspectionResponse.getScope()).contains(limitedScope);
    }
}
