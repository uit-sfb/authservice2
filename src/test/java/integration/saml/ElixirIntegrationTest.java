package integration.saml;

import com.fasterxml.jackson.annotation.JsonProperty;
import integration.IntegrationTestFixture;
import no.uit.metapipe.auth.api.IntrospectionResponse;
import no.uit.metapipe.auth.api.UserInfo;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ElixirAttributes {
    @JsonProperty("urn:oid:1.3.6.1.4.1.5923.1.1.1.13")
    public Attribute elixirIdentifier = new Attribute("abcd123@elixir-europe.org");

    @JsonProperty("urn:oid:2.16.840.1.113730.3.1.241")
    public Attribute displayName = new Attribute("John Doe");

    @JsonProperty("urn:oid:0.9.2342.19200300.100.1.3")
    public Attribute mail = new Attribute("john.doe@example.com");
}

public class ElixirIntegrationTest {
    String clientId = "system_web";
    String clientSecret = "74003f18714b8a46a265";

    @ClassRule
    public static IntegrationTestFixture RULE = new IntegrationTestFixture();

    private static final String mockIdpURL = "http://localhost:9999";
    private final ElixirAttributes elixirAttributes = new ElixirAttributes();

    private Client client;
    private WebDriver driver;

    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
        client.property(ClientProperties.FOLLOW_REDIRECTS, "true");

        int status = -1;
        try {
            Response response = client.target(mockIdpURL + "/api/attributes").request().put(Entity.json(elixirAttributes));
            status = response.getStatus();
        } catch (ProcessingException e) {
            //e.printStackTrace();
        }
        org.junit.Assume.assumeTrue(status == 204);

        driver = new FirefoxDriver();
    }

    @After
    public void tearDown() {
        this.client.close();
        if(this.driver != null) this.driver.quit();
    }

    void verifyAccessTokenAndValidateAttributes(String accessToken) {
        IntrospectionResponse introspectionResponse = RULE.introspect(accessToken);
        assertThat(introspectionResponse.isActive()).isTrue();
        assertThat(introspectionResponse.getSub()).isNotNull();

        UserInfo userInfo = client.target(String.format("http://localhost:%d/oauth2/userinfo", RULE.getLocalPort()))
                .request().header("Authorization", String.format("Bearer %s", accessToken))
                .get(UserInfo.class);
        assertThat(userInfo.getName()).isEqualTo("John Doe");
        assertThat(userInfo.getEmail()).isEqualTo("john.doe@example.com");
        assertThat(userInfo.getSub()).isNotNull();
    }

    void login() {
        WebElement elm = driver.findElement(By.name("j_username"));
        elm.sendKeys("user");
        elm = driver.findElement(By.name("j_password"));
        elm.sendKeys("secret");
        elm = driver.findElement(By.name("submit"));
        elm.click();
    }

    @Test
    public void authorizationCodeFlow() throws Exception {
        int port = RULE.getLocalPort();

        // ============== Obtain the authorization code ==============
        String authUrl = String.format("http://localhost:%d/oauth2/authorize?" +
                "response_type=code&client_id=%s&redirect_uri=http://localhost:%d", port, clientId, port);

        driver.get(authUrl);
        login();

        Thread.sleep(2000);

        URI uri = URI.create(driver.getCurrentUrl());

        String authorizationCode = null;
        List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");
        for(NameValuePair pair : params) {
            if (pair.getName().equals("code")) {
                authorizationCode = pair.getValue();
            }
        }
        assertThat(authorizationCode).isNotNull();

        // ============== Obtain the access token ==============
        OAuthClientRequest tokenRequest = RULE.tokenRequestBuilder()
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .setCode(authorizationCode)
                .setGrantType(GrantType.AUTHORIZATION_CODE)
                .setRedirectURI(String.format("http://localhost:%d", port))
                .buildQueryMessage();
        OAuthAccessTokenResponse tokenResponse = RULE.getOauthClient().accessToken(tokenRequest);
        String accessToken = tokenResponse.getAccessToken();
        assertThat(accessToken).isNotNull();

        // ============== Verify the access token and validate attributes ==============
        verifyAccessTokenAndValidateAttributes(accessToken);
    }

    @Test
    public void implicitGrant() throws Exception {
        String authUrl = String.format("http://localhost:%d/oauth2/authorize?" +
                "response_type=token&client_id=%s&redirect_uri=http://localhost:%d", RULE.getLocalPort(), clientId, 8080);
        driver.get(authUrl);

        Thread.sleep(1000);
        login();
        Thread.sleep(2000);
        URI uri = URI.create(driver.getCurrentUrl());
        String fragment = uri.getRawFragment();

        String accessToken = null;
        List<NameValuePair> params = URLEncodedUtils.parse(fragment, Charset.defaultCharset());
        for(NameValuePair pair : params) {
            if (pair.getName().equals("access_token")) {
                accessToken = pair.getValue();
            }
        }
        assertThat(accessToken).isNotNull();

        verifyAccessTokenAndValidateAttributes(accessToken);
    }

    @Test
    public void authorizationEndpointShouldNeverRedirectToNonWhitelistedUri() {
        //assert(false);
    }
}
