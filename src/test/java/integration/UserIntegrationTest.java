package integration;

import fixtures.FixtureData;
import io.dropwizard.testing.junit.DropwizardAppRule;
import no.uit.metapipe.auth.api.UserDto;
import no.uit.metapipe.auth.api.UsernamePasswordDto;
import no.uit.metapipe.auth.config.MetapipeAuthServiceConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class UserIntegrationTest {
    private Client client;

    @ClassRule
    public static DropwizardAppRule<MetapipeAuthServiceConfiguration> RULE = new IntegrationTestFixture();

    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
    }

    @After
    public void tearDown() {
        this.client.close();
    }

    @Test
    public void user_and_password_registration() {
        // create user
        String validUsername = "validUsernameR";
        Response response = client.target(String.format("http://localhost:%d/u?username=%s", RULE.getLocalPort(), validUsername))
                .request()
                .post(null);
        assertThat(response.getStatus()).isEqualTo(200);

        // verify that user exists
        UserDto userDto = client.target(String.format("http://localhost:%d/u/%s", RULE.getLocalPort(), validUsername))
                .request()
                .get(UserDto.class);
        assertThat(userDto.getUsername()).isEqualTo(validUsername);
    }

    @Test
    public void should_be_able_to_register_users_with_password() {
        FixtureData fixtureData = FixtureData.getDefault();
        Response response = client.target(String.format("http://localhost:%d/u/register", RULE.getLocalPort()))
                .request().post(Entity.json(fixtureData.getRegisteredUsers()));
        assertThat(response.getStatus()).isEqualTo(201);

        for(UsernamePasswordDto up : fixtureData.getRegisteredUsers()) {
            response = client.target(String.format("http://localhost:%d/u/%s", RULE.getLocalPort(), up.getUsername()))
                    .request().get();
            assertThat(response.getStatus()).isEqualTo(200);
        }
    }

    @Test
    public void getUserListSuccessful() {
        FixtureData fixtureData = FixtureData.getDefault();

        ArrayList<UsernamePasswordDto> users = new ArrayList<>();
        users.addAll(fixtureData.getRegisteredUsers());

        Response response = client.target(String.format("http://localhost:%d/u/users?name=%s", RULE.getLocalPort(), users.get(0)))
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        response = client.target(String.format("http://localhost:%d/u/users?name=%s&name=%s", RULE.getLocalPort(),
                users.get(0), users.get(1)))
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        response = client.target(String.format("http://localhost:%d/u/users?name=%s&name=%s&name=%s",
                RULE.getLocalPort(),
                users.get(0), users.get(1), users.get(2)))
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }
}
