create table PasswordDetails (
    id  bigserial not null,
    passwordHash bytea,
    passwordSalt bytea,
    primary key (id)
);

create table Scope (
    id  bigserial not null,
    uri varchar(255) not null,
    token_id int8 not null,
    primary key (id)
);

create table Scope_methods (
    Scope_id int8 not null,
    methods varchar(255) not null,
    primary key (Scope_id, methods)
);

create table Tokens (
    id  bigserial not null,
    active boolean not null,
    tokenId varchar(255) not null,
    secretHash bytea not null,
    createdBy_id int8 not null,
    subject_id int8 not null,
    refreshToken boolean not null,
    primary key (id)
);

create table Users (
    id  bigserial not null,
    username varchar(255),
    passwordDetails_id int8,
    primary key (id)
);

alter table Tokens
    add constraint UK_tokenId  unique (tokenId);

alter table Users
    add constraint UK_username  unique (username);

alter table Scope
    add constraint FK_token_id
    foreign key (token_id)
    references Tokens;

alter table Scope_methods
    add constraint FK_scope_id
    foreign key (Scope_id)
    references Scope;

alter table Tokens
    add constraint FK_createdBy_id
    foreign key (createdBy_id)
    references Users;

alter table Tokens
    add constraint FK_subject_id
    foreign key (subject_id)
    references Users;

alter table Users
    add constraint FK_passwordDetails_id
    foreign key (passwordDetails_id)
    references PasswordDetails;
