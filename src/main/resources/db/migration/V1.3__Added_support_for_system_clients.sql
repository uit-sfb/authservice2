alter table Tokens
    alter column createdBy_id
    drop not null;

alter table Tokens
    alter column subject_id
    drop not null;

alter table  Tokens
    add column systemSubject varchar(255);