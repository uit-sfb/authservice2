create table AuthenticationEvent (
    id  bigserial not null,
    issuer varchar(255),
    externalUserId_id int8,
    primary key (id)
);

create table AuthenticationEvent_attributes (
    AuthenticationEvent_id int8 not null,
    attributes varchar(255),
    attributes_KEY varchar(255),
    primary key (AuthenticationEvent_id, attributes_KEY)
);

create table AuthorizationCodes (
    id  bigserial not null,
    authorizationCode varchar(255),
    state varchar(255),
    used boolean not null,
    validUntil timestamp,
    token_id int8,
    primary key (id)
);

create table ExternalUserId (
    id  bigserial not null,
    identifier varchar(255),
    issuer varchar(255),
    user_id int8,
    primary key (id)
);

create table ExternalUserId_AuthenticationEvent (
    ExternalUserId_id int8 not null,
    authenticationEvents_id int8 not null,
    primary key (ExternalUserId_id, authenticationEvents_id)
);

alter table ExternalUserId_AuthenticationEvent
    add constraint UK_96th4hm72ny4rpscgf1r0dn7f  unique (authenticationEvents_id);

alter table AuthenticationEvent
    add constraint FK_cxoh3y512ji2txigrig0ufod0
    foreign key (externalUserId_id)
    references ExternalUserId;

alter table AuthenticationEvent_attributes
    add constraint FK_mlp7yan0qqsfpdxvxfh8fvmmg
    foreign key (AuthenticationEvent_id)
    references AuthenticationEvent;

alter table AuthorizationCodes
    add constraint FK_mnb3ai4pcrbqm8uydi3nv4g42
    foreign key (token_id)
    references Tokens;


alter table ExternalUserId
    add constraint FK_ic6covdggdapc8wua7jdwjlv9
    foreign key (user_id)
    references Users;

alter table ExternalUserId_AuthenticationEvent
    add constraint FK_96th4hm72ny4rpscgf1r0dn7f
    foreign key (authenticationEvents_id)
    references AuthenticationEvent;

alter table ExternalUserId_AuthenticationEvent
    add constraint FK_r2grhlwwl4hpy2v511raxwgy2
    foreign key (ExternalUserId_id)
    references ExternalUserId;


-- tokens!!!
--
alter table  Tokens
    add column authenticationEvent_id int8;

alter table Tokens
    add constraint FK_p70t7moup6hp3epfllrpv8rss
    foreign key (authenticationEvent_id)
    references AuthenticationEvent;