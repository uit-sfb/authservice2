<#-- @ftlvariable name="" type="no.uit.metapipe.auth.view.LoginFormView" -->

<html>
    <body>
        <form action="${getAction()?html}" method="post">
            <div>
                <label for="name">Username:</label>
                <input type="text" name="username" />
            </div>
            <div>
                <label for="mail">Password:</label>
                <input type="password" name="password" />
            </div>
            <input type="hidden" name="redirectSuccess" value="${getRedirectSuccess()?html}" />
            <input type="hidden" name="redirectFailure" value="${getRedirectFailure()?html}" />
            <div class="button">
                <button type="submit">Login</button>
            </div>
        </form>
    </body>
</html>