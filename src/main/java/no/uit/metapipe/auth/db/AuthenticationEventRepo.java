package no.uit.metapipe.auth.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.auth.core.AuthenticationEvent;
import no.uit.metapipe.auth.core.ExternalUserId;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class AuthenticationEventRepo extends AbstractDAO<AuthenticationEvent> {
    public AuthenticationEventRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public AuthenticationEvent saveOrUpdate(AuthenticationEvent authenticationEvent) {
        currentSession().saveOrUpdate(authenticationEvent);
        return authenticationEvent;
    }

    public Optional<List<AuthenticationEvent>> findByExternalUserId(ExternalUserId externalUserId) {
        Query q = currentSession().getNamedQuery("findByExternalUserId").setParameter("extUserId", externalUserId);
        List<AuthenticationEvent> authenticationEvents = list(q);
        if(authenticationEvents.size() == 0) {
            return Optional.empty();
        } else {
            return Optional.of(authenticationEvents);
        }
    }
}
