package no.uit.metapipe.auth.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.auth.core.Scope;
import org.hibernate.SessionFactory;

public class ScopeRepo extends AbstractDAO<Scope> {
    public ScopeRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Scope saveOrUpdate(Scope scope) {
        currentSession().saveOrUpdate(scope);
        return scope;
    }
}
