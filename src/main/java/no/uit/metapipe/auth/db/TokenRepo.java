package no.uit.metapipe.auth.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.auth.core.Token;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

public class TokenRepo extends AbstractDAO<Token> {
    private static Logger logger = LoggerFactory.getLogger(TokenRepo.class);

    public TokenRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Token> findByTokenId(String tokenId) {
        org.hibernate.Session s = currentSession();
        Query q = s.getNamedQuery("findByTokenId")
                .setParameter("tokenId", tokenId);
        List<Token> list = list(q);
        if(list.size() > 0) {
            return Optional.of(list.get(0));
        } else {
            return Optional.empty();
        }
    }

    public Token saveOrUpdate(Token token) {
        currentSession().saveOrUpdate(token);
        return token;
    }

    public Token save(Token token) {
        currentSession().save(token);
        return token;
    }

    public Token update(Token token) {
        currentSession().update(token);
        return token;
    }

    public void delete(Token token) {
        org.hibernate.Session s = currentSession();
        s.delete(token);
    }
}
