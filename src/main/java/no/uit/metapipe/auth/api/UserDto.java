package no.uit.metapipe.auth.api;

public class UserDto {
    String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
