package no.uit.metapipe.auth.api;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessToken {
    @JsonIgnore
    private String tokenId;
    @JsonIgnore
    private String tokenSecret;

    public AccessToken(String tokenId, String tokenSecret) {
        this.tokenId = tokenId;
        this.tokenSecret = tokenSecret;
    }

    protected AccessToken() {

    }

    public static AccessToken fromString(String token) {
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(token);
        return accessToken;
    }

    @JsonProperty
    public String getToken() {
        return this.tokenId + "." + this.tokenSecret;
    }

    public void setToken(String token) {
        String[] parts = token.split("\\.");
        tokenId = parts[0];
        tokenSecret = parts[1];
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }
}
