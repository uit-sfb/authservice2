package no.uit.metapipe.auth.view;

import io.dropwizard.views.View;

public class UserRegistrationFormView extends View {
    private String redirectSuccess;
    private String redirectFailure;
    private String action;

    public UserRegistrationFormView() {
        super("UserRegistrationForm.ftl");
    }

    public String getRedirectSuccess() {
        return redirectSuccess;
    }

    public void setRedirectSuccess(String redirectSuccess) {
        this.redirectSuccess = redirectSuccess;
    }

    public String getRedirectFailure() {
        return redirectFailure;
    }

    public void setRedirectFailure(String redirectFailure) {
        this.redirectFailure = redirectFailure;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
