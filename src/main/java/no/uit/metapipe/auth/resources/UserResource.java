package no.uit.metapipe.auth.resources;

import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import no.uit.metapipe.auth.api.UserDto;
import no.uit.metapipe.auth.api.UserInfo;
import no.uit.metapipe.auth.api.UsernamePasswordDto;
import no.uit.metapipe.auth.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.AuthenticationException;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Path("/u")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    private UserService userService;
    private static Logger logger = LoggerFactory.getLogger(UserResource.class);

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @POST
    @Timed
    @UnitOfWork
    public UserDto create(@QueryParam("username") String username) {
        return userService.createUser(username);
    }

    @GET
    @UnitOfWork
    @Timed
    public List<UserDto> listUsers() {
        return userService.listUsers();
    }

    @GET
    @Path("{username}")
    @UnitOfWork
    public UserDto getUser(@PathParam("username") String username) {
        Optional<UserDto> userDtoOptional = userService.findByUsername(username);
        if (userDtoOptional.isPresent()) {
            return userDtoOptional.get();
        } else throw new NotFoundException("no such user");
    }

    @POST
    @Path("/register")
    @UnitOfWork
    @Timed
    public Response registerUsers(List<UsernamePasswordDto> usernamePasswordList) {
        List<UserDto> result = new ArrayList<>(usernamePasswordList.size());
        for (UsernamePasswordDto up : usernamePasswordList) {
            UserDto userDto = userService.registerUsernamePassword(up.getUsername(), up.getPassword());
            result.add(userDto);
        }
        return Response.status(Response.Status.CREATED)
                .entity(result)
                .build();
    }

    @GET
    @Path("/users")
    @UnitOfWork
    public Response getUsersFromQuery(@Context UriInfo uriInfo, @Context HttpHeaders headers) {
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        List<String> userNames = queryParams.get("name");
        try {
            if (headers.getHeaderString("Content-Type") != null) {
                if (!headers.getHeaderString("Content-Type").equalsIgnoreCase(MediaType.APPLICATION_FORM_URLENCODED)) {
                    return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE)
                            .build();
                }
            }
            List<UserInfo> users = userService.getUsersFromNames(userNames);
            return Response.status(Response.Status.OK)
                    .entity(users)
                    .build();
        } catch (AuthenticationException e) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .build();
        }
    }
}
