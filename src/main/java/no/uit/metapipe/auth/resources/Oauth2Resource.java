package no.uit.metapipe.auth.resources;

import io.dropwizard.hibernate.UnitOfWork;
import no.uit.metapipe.auth.api.IntrospectionResponse;
import no.uit.metapipe.auth.api.UserInfo;
import no.uit.metapipe.auth.core.*;
import no.uit.metapipe.auth.db.ClientRepo;
import no.uit.metapipe.auth.service.SamlAuthorizationService;
import no.uit.metapipe.auth.service.TokenService;
import no.uit.metapipe.auth.service.grants.util.AuthorizationGrantService;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;
import no.uit.metapipe.auth.utils.OAuthRequestWrapper;
import org.apache.commons.lang.NotImplementedException;
import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.oltu.oauth2.common.message.types.ParameterStyle;
import org.apache.oltu.oauth2.rs.request.OAuthAccessResourceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Path("/oauth2")
public class Oauth2Resource {

    private final TokenService tokenService;
    private final AuthorizationGrantService authorizationGrantService;
    private final SamlAuthorizationService samlAuthorizationService;
    private final ClientRepo clientRepo;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Oauth2Resource(TokenService tokenService, AuthorizationGrantService authorizationGrantService, SamlAuthorizationService samlAuthorizationService, ClientRepo clientRepo) {
        this.tokenService = tokenService;
        this.authorizationGrantService = authorizationGrantService;
        this.samlAuthorizationService = samlAuthorizationService;
        this.clientRepo = clientRepo;
    }

    protected Response invalidRequest(String description) throws OAuthSystemException {
        OAuthResponse response = OAuthASResponse
                .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.INVALID_REQUEST)
                .setErrorDescription(description)
                .buildJSONMessage();
        return getResponse(response);
    }

    protected Response invalidGrant(String description) throws OAuthSystemException {
        OAuthResponse response = OAuthASResponse
                .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.INVALID_GRANT)
                .setErrorDescription(description)
                .buildJSONMessage();
        return getResponse(response);
    }

    protected Response getResponse(OAuthResponse response) {
        return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
    }


    @GET
    @Path("/authorize")
    @UnitOfWork
    public Response authorize(@Context HttpServletRequest servletRequest, @Context UriInfo uriInfo) throws OAuthSystemException, Exception {
        try {
            OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(new OAuthRequestWrapper(servletRequest, uriInfo.getQueryParameters()));
            /*
               RFC 6749, chapter 4.1.2.1.:
                   If the request fails due to a missing, invalid, or mismatching
                   redirection URI, or if the client identifier is missing or invalid,
                   the authorization server SHOULD inform the resource owner of the
                   error and MUST NOT automatically redirect the user-agent to the
                   invalid redirection URI.

                   If the resource owner denies the access request or if the request
                   fails for reasons other than a missing or invalid redirection URI,
                   the authorization server informs the client
             */

            // Todo: this is a dangerous mess. We should move this code into a separate service and have proper unit tests.

            Optional<Client> clientOptional = clientRepo.findByClientId(oauthRequest.getClientId());
            if (!clientOptional.isPresent()) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity("Invalid client.").build();
            }

            Client client = clientOptional.get();

            boolean isAuthorized = false; // = ;
            switch (oauthRequest.getResponseType()) {
                case "code":
                    if(client.hasAuthorizedGrant(GrantType.AUTHORIZATION_CODE.toString())) {
                        isAuthorized = true;
                    }
                    break;
                case "token":
                    if(client.hasAuthorizedGrant(GrantType.IMPLICIT.toString())) {
                        isAuthorized = true;
                    }
                    break;
                default:
                    isAuthorized = false;
            }


            boolean canRedirect = client.canRedirectTo(oauthRequest.getRedirectURI());

            OAuthResponse res;

            if(isAuthorized && canRedirect) {
                switch (oauthRequest.getResponseType()) {
                    case "code":
                        AuthorizationCode authorizationCode = samlAuthorizationService.createAuthorizationCode(oauthRequest.getScopes());
                        res = OAuthASResponse.authorizationResponse(servletRequest, HttpServletResponse.SC_FOUND)
                                .setCode(authorizationCode.getAuthorizationCode())
                                .location(oauthRequest.getRedirectURI())
                                .buildQueryMessage();
                        break;
                    case "token":
                        OAuthResponseDetails responseDetails = samlAuthorizationService.getAccessToken(oauthRequest.getScopes(), false);
                        res = OAuthASResponse.authorizationResponse(servletRequest, HttpServletResponse.SC_FOUND)
                                .setAccessToken(responseDetails.getAccessToken())
                                .location(oauthRequest.getRedirectURI())
                                .buildQueryMessage();
                        break;
                    default:
                        throw new NotImplementedException();
                        // todo: return invalid grant
                }
            } else {
                if(!isAuthorized && canRedirect) {
                    res = OAuthASResponse.errorResponse(HttpServletResponse.SC_FOUND)
                            .setState(oauthRequest.getState())
                            .setError("unauthorized_client")
                            .location(oauthRequest.getRedirectURI())
                            .buildQueryMessage();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST)
                            .entity("Error: Redirect url is not in client whitelist").build();
                }
            }
            return Response.temporaryRedirect(URI.create(res.getLocationUri())).status(res.getResponseStatus()).build();
        } catch (OAuthProblemException e) {
            e.printStackTrace();
            return Response.status(501).entity("501 something went wrong, sorry").build();
        }
    }


    @Path("/token")
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response token(@Context HttpServletRequest servletRequest, MultivaluedMap<String, String> form) throws OAuthSystemException {
        Optional<String> accessTokenOptional = Optional.empty();
        OAuthTokenRequest tokenRequest;
        if(servletRequest == null) {
            throw new IllegalArgumentException("servletRequest cannot be null");
        }
        try {
            tokenRequest = new OAuthTokenRequest(new OAuthRequestWrapper(servletRequest, form));
        } catch (OAuthProblemException e) {
            OAuthResponse res = OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST).error(e)
                    .buildJSONMessage();
            return Response.status(res.getResponseStatus()).entity(res.getBody()).build();
        }
        return getResponse(authorizationGrantService.authorize(tokenRequest));
    }


    @Path("/revoke")
    @POST
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public Response revoke(@FormParam("token") String token) {
        tokenService.invalidateByAccessToken(token);
        return Response.ok().build();
    }

    // Implements: Oauth 2.0 Token Introspection (rfc7662)
    @Path("/introspect")
    @POST
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public Response introspect(@FormParam("token") String token) {
        IntrospectionResponse introspectionResponse = tokenService.introspect(token);
        return Response.ok(introspectionResponse).build();
    }

    // Implements the "UserInfo Endpoint" as defined in "OpenID Connect Core 1.0 incorporating errata set 1"
    @Path("/userinfo")
    @GET
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public Response userInfo(@Context HttpServletRequest servletRequest) throws Exception {
        OAuthAccessResourceRequest oauthRequest;
        String accessToken;
        try {
            oauthRequest = new OAuthAccessResourceRequest(servletRequest, ParameterStyle.HEADER);
            accessToken = oauthRequest.getAccessToken();
        } catch (Exception e) {
            // todo: invalid request
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity("bad request").build();
        }

        Optional<Token> tokenOptional = tokenService.getByTokenString(accessToken);
        if (!tokenOptional.isPresent()) {
            // todo token does not exist

            throw new Exception("NOOOOOOOOOOOO!!!");
        }

        Token token = tokenOptional.get();
        User user = token.getSubject();
        AuthenticationEvent authenticationEvent = token.getAuthenticationEvent();

        UserInfo userInfo = new UserInfo();
        userInfo.setSub(user.getUsername());
        Map<String, List<String>> attributes = authenticationEvent.getAttributesList();

        userInfo.setEmail(attributes.get("urn:oid:0.9.2342.19200300.100.1.3").get(0));
        userInfo.setName(attributes.get("urn:oid:2.16.840.1.113730.3.1.241").get(0));

        return Response.status(Response.Status.OK).entity(userInfo).build();
    }
}
