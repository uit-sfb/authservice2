package no.uit.metapipe.auth;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.flyway.FlywayBundle;
import io.dropwizard.flyway.FlywayFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import no.uit.metapipe.auth.config.MetapipeAuthServiceConfiguration;
import no.uit.metapipe.auth.core.*;
import no.uit.metapipe.auth.db.*;
import no.uit.metapipe.auth.resources.HelloWorldResource;
import no.uit.metapipe.auth.resources.LoginResource;
import no.uit.metapipe.auth.resources.Oauth2Resource;
import no.uit.metapipe.auth.resources.UserResource;
import no.uit.metapipe.auth.service.*;
import no.uit.metapipe.auth.service.grants.AuthorizationCodeGrant;
import no.uit.metapipe.auth.service.grants.ClientCredentialsGrant;
import no.uit.metapipe.auth.service.grants.PasswordCredentialsGrant;
import no.uit.metapipe.auth.service.grants.RefreshTokenGrant;
import no.uit.metapipe.auth.service.grants.util.AuthorizationGrantDispatcher;
import no.uit.metapipe.auth.utils.TokenFactory;
import no.uit.metapipe.auth.utils.saml.SamlConfigHelper;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class MetapipeAuthServiceApplication extends Application<MetapipeAuthServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MetapipeAuthServiceApplication().run(args);
    }

    private final HibernateBundle<MetapipeAuthServiceConfiguration> hibernateBundle =
            new HibernateBundle<MetapipeAuthServiceConfiguration>(
                    User.class,
                    Token.class,
                    Scope.class,
                    PasswordDetails.class,
                    ExternalUserId.class,
                    AuthenticationEvent.class,
                    AuthorizationCode.class
            ) {
        @Override
        public DataSourceFactory getDataSourceFactory(MetapipeAuthServiceConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public String getName() {
        return "MetapipeAuthService";
    }

    @Override
    public void initialize(final Bootstrap<MetapipeAuthServiceConfiguration> bootstrap) {
        bootstrap.addCommand(new GenerateClientCredCommand(this));
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(new ViewBundle<MetapipeAuthServiceConfiguration>());

        bootstrap.addBundle(new FlywayBundle<MetapipeAuthServiceConfiguration>() {

            @Override
            public DataSourceFactory getDataSourceFactory(MetapipeAuthServiceConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }

            @Override
            public FlywayFactory getFlywayFactory(MetapipeAuthServiceConfiguration configuration) {
                return configuration.getFlywayFactory();
            }
        });
    }

    public void registerSpringSecurity(MetapipeAuthServiceConfiguration configuration, Environment environment) {
        SamlConfigHelper configHelper = new SamlConfigHelper(configuration.getSamlConfig(), configuration.getExternalUrl());
        configHelper.initializeSpringSecurityAndRegisterServletFilters(environment);
    }

    @Override
    public void run(final MetapipeAuthServiceConfiguration configuration,
                    final Environment environment) {

        registerSpringSecurity(configuration, environment);

        configureCORS(environment);

        AuthorizationCodeRepo authorizationCodeRepo = new AuthorizationCodeRepo(hibernateBundle.getSessionFactory());
        UserRepo userRepo = new UserRepo(hibernateBundle.getSessionFactory());
        TokenRepo tokenRepo = new TokenRepo(hibernateBundle.getSessionFactory());
        ScopeRepo scopeRepo = new ScopeRepo(hibernateBundle.getSessionFactory());
        ExternalUserIdRepo externalUserIdRepo = new ExternalUserIdRepo(hibernateBundle.getSessionFactory());
        ClientRepo clientRepo = new ClientRepo(configuration.getOAuthConfig());
        AuthenticationEventRepo authenticationEventRepo = new AuthenticationEventRepo(hibernateBundle.getSessionFactory());

        UserService userService = new UserServiceImpl(userRepo, externalUserIdRepo, authenticationEventRepo);

        TokenFactory tokenFactory = new TokenFactory();
        TokenService tokenService = new TokenServiceImpl(userRepo, tokenRepo, scopeRepo, tokenFactory, configuration.getOAuthConfig());

        AuthorizationService authorizationService = new AuthorizationServiceImpl(scopeRepo, tokenRepo, tokenService, authorizationCodeRepo);
        SamlAuthorizationService samlAuthorizationService = new SamlAuthorizationService(authenticationEventRepo, authorizationCodeRepo, userService, authorizationService, tokenService, tokenRepo);

        AuthorizationGrantDispatcher grantDispatcher = new AuthorizationGrantDispatcher()
                .add(new PasswordCredentialsGrant(clientRepo, authorizationService, userRepo))
                .add(new RefreshTokenGrant(clientRepo, tokenService, authorizationService))
                .add(new AuthorizationCodeGrant(clientRepo, authorizationService))
                .add(new ClientCredentialsGrant(authorizationService, clientRepo));

        environment.jersey().register(new HelloWorldResource());
        environment.jersey().register(new UserResource(userService));
        environment.jersey().register(new LoginResource(configuration.getAuthCookie(), userService, tokenService));

        environment.jersey().register(new Oauth2Resource(tokenService, grantDispatcher, samlAuthorizationService, clientRepo));
    }

    public static void configureCORS(Environment environment) {
        // Solution taken from: http://stackoverflow.com/a/25801822/1390113

        FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin,Authorization,Pragma,Cache-Control");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/oauth2/*");
    }
}
