package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.core.*;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;

import java.util.Optional;
import java.util.Set;

public interface AuthorizationService {
    OAuthResponseDetails authorize(Optional<Client> clientOptional, Optional<User> userOptional, Set<String> scopeSet, boolean getRefreshToken, Optional<AuthenticationEvent> authenticationEvent);
    Set<Scope> scopeIntersection(Set<Scope> allowedScopes, Set<Scope> requestedScope);
    OAuthResponseDetails authorizeCode(String authorizationCode, boolean getRefreshToken); // // TODO: rename token (it maps to token-endpoint, not authorization endpoint)?
    Token refresh(Token refreshToken);
}
