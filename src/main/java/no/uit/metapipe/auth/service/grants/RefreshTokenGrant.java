package no.uit.metapipe.auth.service.grants;

import no.uit.metapipe.auth.core.Client;
import no.uit.metapipe.auth.core.Scope;
import no.uit.metapipe.auth.core.Token;
import no.uit.metapipe.auth.db.ClientRepo;
import no.uit.metapipe.auth.service.AuthorizationService;
import no.uit.metapipe.auth.service.TokenService;
import no.uit.metapipe.auth.service.grants.util.AbstractAuthorizationGrantService;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;
import no.uit.metapipe.auth.utils.OAuthUtils;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;

import java.util.Optional;
import java.util.Set;

public class RefreshTokenGrant extends AbstractAuthorizationGrantService {
    private final ClientRepo clientRepo;
    private final TokenService tokenService;

    private final AuthorizationService authorizationService;

    public RefreshTokenGrant(ClientRepo clientRepo, TokenService tokenService, AuthorizationService authorizationService) {
        super(clientRepo);
        this.clientRepo = clientRepo;
        this.tokenService = tokenService;
        this.authorizationService = authorizationService;
    }


    @Override
    public OAuthResponse authorize(OAuthTokenRequest tokenRequest, Client client) throws OAuthSystemException {

        // todo: add missing test to see that it's actually a refresh token
        // todo: creating a refresh token should not happen here, but in either AuthorizationService or TokenService

        Optional<Token> existingRefreshTokenOptional = tokenService.getByTokenString(tokenRequest.getRefreshToken());
        if (existingRefreshTokenOptional.isPresent()) {
            Token existingRefreshToken = existingRefreshTokenOptional.get();

            Set<Scope> maximumScope = existingRefreshToken.getScopes();
            Set<Scope> requestedScope = OAuthUtils.scopeFromStringSet(tokenRequest.getScopes());

            // This *might* be in violation of the spec. (RFC-6749, §6, "scope").
            Set<Scope> resultingScope = authorizationService.scopeIntersection(maximumScope, requestedScope);
            OAuthResponseDetails responseDetails =
                    authorizationService.authorize(Optional.empty(), Optional.of(existingRefreshToken.getSubject()),
                            OAuthUtils.encodeScope(resultingScope), false, Optional.empty());
            return tokenResponseBuilder(responseDetails).buildJSONMessage();
        } else {
            return invalidRefreshToken();
        }
    }

    @Override
    public String getGrantType() {
        return GrantType.REFRESH_TOKEN.toString();
    }

    private OAuthResponse invalidRefreshToken() throws OAuthSystemException {
        return invalidGrant("invalid refresh token");
    }
}
