package no.uit.metapipe.auth.service.grants.util;

import no.uit.metapipe.auth.api.AccessToken;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

import java.util.Optional;

public interface AuthorizationGrantService {
    OAuthResponse authorize(OAuthTokenRequest tokenRequest) throws OAuthSystemException;
    String getGrantType();
}
