package no.uit.metapipe.auth.service.grants;

import no.uit.metapipe.auth.core.Client;
import no.uit.metapipe.auth.core.PasswordDetails;
import no.uit.metapipe.auth.core.User;
import no.uit.metapipe.auth.db.ClientRepo;
import no.uit.metapipe.auth.db.UserRepo;
import no.uit.metapipe.auth.service.AuthorizationService;
import no.uit.metapipe.auth.service.grants.util.AbstractAuthorizationGrantService;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;

import java.util.Optional;

public class PasswordCredentialsGrant extends AbstractAuthorizationGrantService {
    final private ClientRepo clientRepo;
    final private AuthorizationService authorizationService;
    final private UserRepo userRepo;

    public PasswordCredentialsGrant(ClientRepo clientRepo, AuthorizationService authorizationService, UserRepo userRepo) {
        super(clientRepo);
        this.clientRepo = clientRepo;
        this.authorizationService = authorizationService;
        this.userRepo = userRepo;
    }

    @Override
    public OAuthResponse authorize(OAuthTokenRequest tokenRequest, Client client) throws OAuthSystemException {
        User subject = userRepo.findByUsername(tokenRequest.getUsername());
        PasswordDetails passwordDetails = subject.getPasswordDetails();
        if(passwordDetails.isValidPassword(tokenRequest.getPassword())) {
            OAuthResponseDetails responseDetails = authorizationService.authorize(Optional.empty(), Optional.of(subject), tokenRequest.getScopes(), true, Optional.empty());
            return tokenResponseBuilder(responseDetails).buildJSONMessage();
        } else {
            return invalidGrant("invalid username or password");
        }
    }

    @Override
    public String getGrantType() {
        return GrantType.PASSWORD.toString();
    }
}
