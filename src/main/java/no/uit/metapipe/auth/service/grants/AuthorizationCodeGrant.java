package no.uit.metapipe.auth.service.grants;

import no.uit.metapipe.auth.core.Client;
import no.uit.metapipe.auth.db.ClientRepo;
import no.uit.metapipe.auth.service.AuthorizationService;
import no.uit.metapipe.auth.service.grants.util.AbstractAuthorizationGrantService;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

public class AuthorizationCodeGrant extends AbstractAuthorizationGrantService {
    private final ClientRepo clientRepo;
    private final AuthorizationService authorizationService;

    public AuthorizationCodeGrant(ClientRepo clientRepo, AuthorizationService authorizationService) {
        super(clientRepo);
        this.clientRepo = clientRepo;
        this.authorizationService = authorizationService;
    }

    @Override
    public OAuthResponse authorize(OAuthTokenRequest tokenRequest, Client client) throws OAuthSystemException {
        OAuthResponseDetails responseDetails = authorizationService.authorizeCode(tokenRequest.getCode(), true);
        return tokenResponseBuilder(responseDetails).buildJSONMessage();
    }

    @Override
    public String getGrantType() {
        return "authorization_code";
    }
}
