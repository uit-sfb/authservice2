package no.uit.metapipe.auth.service.grants.util;

import no.uit.metapipe.auth.core.Client;
import no.uit.metapipe.auth.db.ClientRepo;
import no.uit.metapipe.auth.utils.OAuthUtils;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public abstract class AbstractAuthorizationGrantService implements AuthorizationGrantService {
    private final ClientRepo clientRepo;

    protected AbstractAuthorizationGrantService(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    protected OAuthResponse invalidGrant(String description) throws OAuthSystemException {
        return OAuthASResponse
                .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.INVALID_GRANT)
                .setErrorDescription(description)
                .buildJSONMessage();
    }

    protected OAuthASResponse.OAuthTokenResponseBuilder tokenResponseBuilder(OAuthResponseDetails responseDetails) {
        OAuthASResponse.OAuthTokenResponseBuilder builder = OAuthASResponse
                .tokenResponse(HttpServletResponse.SC_OK)
                .setTokenType("Bearer")
                .setAccessToken(responseDetails.getAccessToken());

        if (responseDetails.getRefreshTokenOptional().isPresent()) {
            builder.setRefreshToken(responseDetails.getRefreshTokenOptional().get());
        }

        builder.setScope(OAuthUtils.buildScope(responseDetails.getScope()));

        return builder;
    }

    public OAuthResponse authorize(OAuthTokenRequest request) throws OAuthSystemException {
        Optional<Client> clientOptional = clientRepo.findByClientIdAndSecret(request.getClientId(), request.getClientSecret());
        if(!clientOptional.isPresent()) {
            return OAuthASResponse
                    .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                    .setError(OAuthError.TokenResponse.INVALID_CLIENT)
                    .setErrorDescription("invalid client id or secret")
                    .buildJSONMessage();
        }
        Client client = clientOptional.get();
        if(!client.hasAuthorizedGrant(getGrantType())) {
            return OAuthASResponse
                    .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                    .setError(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT)
                    .setErrorDescription("grant \"" + request.getGrantType() + "\" not allowed for this client")
                    .buildJSONMessage();
        }
        return authorize(request, client);
    }

    public abstract OAuthResponse authorize(OAuthTokenRequest request, Client client) throws OAuthSystemException;
}
