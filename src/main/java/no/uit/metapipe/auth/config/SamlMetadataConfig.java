package no.uit.metapipe.auth.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.saml.metadata.ExtendedMetadata;

public class SamlMetadataConfig {
    String location;

    @JsonProperty
    ExtendedMetadata extendedMetadata;

    public String getLocation() {
        return location;
    }

    public ExtendedMetadata getExtendedMetadata() {
        return extendedMetadata;
    }
}
