package no.uit.metapipe.auth.utils;

import org.apache.commons.ssl.util.Hex;

import java.security.SecureRandom;
import java.util.Base64;

public class SecretsGenerator {
    private final SecureRandom secureRandom = new SecureRandom();

    public String createSecret(int size) {
        byte[] bytes = new byte[size];
        secureRandom.nextBytes(bytes);
        return Hex.encode(bytes);
    }
}
