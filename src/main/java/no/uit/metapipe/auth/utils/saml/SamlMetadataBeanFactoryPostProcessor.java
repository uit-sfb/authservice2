package no.uit.metapipe.auth.utils.saml;

import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.security.saml.context.SAMLContextProviderLB;
import org.springframework.security.saml.metadata.MetadataManager;

import java.net.URI;
import java.util.List;

public class SamlMetadataBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    private final BeanPostProcessor postProcessor;
    private final String externalUrl;

    public SamlMetadataBeanFactoryPostProcessor(List<MetadataProvider> providers, String externalUrl) {
        this.externalUrl = externalUrl;

        postProcessor = new BeanPostProcessor() {
            @Override
            public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
                if(beanName.equals("metadata")) {
                    MetadataManager metadataManager = (MetadataManager) bean;
                    try {
                        metadataManager.setProviders(providers);
                    } catch (MetadataProviderException e) {
                        throw new IllegalStateException("The list of providers is not valid", e);
                    }
                }
                if(beanName.equals("contextProvider")) {
                    SAMLContextProviderLB contextProvider = (SAMLContextProviderLB) bean;
                    URI  uri = URI.create(SamlMetadataBeanFactoryPostProcessor.this.externalUrl);
                    contextProvider.setScheme(uri.getScheme());
                    contextProvider.setServerName(uri.getHost());
                    int port = uri.getPort();

                    if (port == -1){
                        port = 0;
                        contextProvider.setIncludeServerPortInRequestURL(false);
                    } else {
                        contextProvider.setIncludeServerPortInRequestURL(true);
                    }

                    contextProvider.setServerPort(port);
                    contextProvider.setContextPath(uri.getPath());
                }
                return bean;
            }

            @Override
            public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
                return bean;
            }
        };
    }


    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        beanFactory.addBeanPostProcessor(this.postProcessor);
    }
}
