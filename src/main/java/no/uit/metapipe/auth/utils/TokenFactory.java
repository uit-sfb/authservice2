package no.uit.metapipe.auth.utils;

import no.uit.metapipe.auth.core.Token;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.UUID;

public class TokenFactory {
    private final SecureRandom secureRandom = new SecureRandom();

    private String createSecret() {
        byte[] bytes = new byte[64];
        secureRandom.nextBytes(bytes);

        return Base64.getUrlEncoder().encodeToString(bytes);
    }

    public Token createToken() {
        Token s = new Token();
        s.setTokenId(UUID.randomUUID().toString());
        return s;
    }
}
