package no.uit.metapipe.auth.utils.saml;

import com.fasterxml.jackson.annotation.JsonProperty;
import no.uit.metapipe.auth.config.SamlMetadataConfig;
import org.springframework.security.saml.metadata.MetadataGenerator;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

public class SamlConfig {
    @Valid
    @NotNull
    @JsonProperty
    KeyStoreConfig keyStore = new KeyStoreConfig();

    @Valid
    @NotNull
    @JsonProperty
    MetadataGenerator metadataGenerator = new MetadataGenerator();

    @Valid
    @NotNull
    @JsonProperty
    List<SamlMetadataConfig> metadata = new LinkedList<>();

    public KeyStoreConfig getKeyStoreConfig() {
        return keyStore;
    }

    public MetadataGenerator getMetadataGenerator() {
        return metadataGenerator;
    }

    public List<SamlMetadataConfig> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<SamlMetadataConfig> metadata) {
        this.metadata = metadata;
    }
}
