package no.uit.metapipe.auth.utils.saml;

import io.dropwizard.setup.Environment;
import org.eclipse.jetty.server.session.SessionHandler;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractRefreshableConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.security.saml.key.JKSKeyManager;
import org.springframework.security.saml.metadata.MetadataGenerator;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.DispatcherType;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.security.KeyStore;
import java.util.EnumSet;
import java.util.List;

public class SamlConfigHelper {
    private final SamlConfig config;
    private final String externalUrl;

    public SamlConfigHelper(SamlConfig config, String externalUrl) {
        this.config = config;
        this.externalUrl = externalUrl;
    }

    public KeyStore getKeystore() {
        KeyStoreConfig keyStoreConfig = config.getKeyStoreConfig();
        URI keystoreLocation = URI.create(keyStoreConfig.getLocation());
        KeyStore keyStore;
        try {
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream inputStream;
            if(keystoreLocation.getScheme() == null) {
                inputStream = new FileInputStream(keyStoreConfig.getLocation());
            } else {
                switch (keystoreLocation.getScheme()) {
                    case "classpath":
                        inputStream = getClass().getResourceAsStream(keystoreLocation.getPath());
                        break;
                    case "file":
                        inputStream = new FileInputStream(keystoreLocation.getPath());
                        break;
                    default:
                        throw new IllegalArgumentException("Keystore location must be either file:// or classpath://");
                }
            }
            try {
                keyStore.load(inputStream, keyStoreConfig.getPassword().toCharArray());
            } finally {
                inputStream.close();
            }
        } catch (Exception e) {
            throw new IllegalStateException("Failed to load KeyStore", e);
        }
        return keyStore;
    }

    public AbstractRefreshableConfigApplicationContext createApplicationContext() {
        GenericApplicationContext parent = new GenericApplicationContext();
        parent.refresh();

        KeyStoreConfig keyStoreConfig = config.getKeyStoreConfig();

        JKSKeyManager keyManager = new JKSKeyManager(getKeystore(), keyStoreConfig.getPasswords(), keyStoreConfig.getDefaultKey());
        parent.getBeanFactory().registerSingleton("keyManager", keyManager);

        MetadataGenerator metadataGenerator = config.getMetadataGenerator();
        metadataGenerator.setKeyManager(keyManager);
        parent.getBeanFactory().registerSingleton("metadataGenerator", metadataGenerator);

        return new ClassPathXmlApplicationContext(new String[]{"security.xml"}, parent);
    }

    public void applyTo(AbstractRefreshableConfigApplicationContext context) {
        SamlMetdataProviderFactory samlMetdataProviderFactory = SamlMetdataProviderFactory.fromApplicationContext(context);
        List<MetadataProvider> providers = samlMetdataProviderFactory.createFrom(config.getMetadata());
        SamlMetadataBeanFactoryPostProcessor postProcessor = new SamlMetadataBeanFactoryPostProcessor(providers, externalUrl);
        context.addBeanFactoryPostProcessor(postProcessor);
        context.refresh();
    }

    public void registerServlets(ApplicationContext context, Environment environment) {
        final String DEFAULT_FILTER_BEAN_NAME = "org.springframework.security.filterChainProxy";
        if(environment.getAdminContext().getSessionHandler() == null) {
            environment.getApplicationContext().setSessionHandler(new SessionHandler());
        }
        FilterChainProxy proxy = (FilterChainProxy) context.getBean(DEFAULT_FILTER_BEAN_NAME);
        environment.servlets().addFilter("filterChain", new DelegatingFilterProxy(proxy)).addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    }

    public void initializeSpringSecurityAndRegisterServletFilters(Environment environment) {
        AbstractRefreshableConfigApplicationContext context = createApplicationContext();
        applyTo(context);
        registerServlets(context, environment);
    }
}
