package no.uit.metapipe.auth.core;


import javax.persistence.*;
import java.util.*;

@Entity
@NamedQuery(
        name = "findByExternalUserId",
        query = "SELECT a FROM AuthenticationEvent a WHERE a.externalUserId = :extUserId"
)
public class AuthenticationEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ManyToOne
    ExternalUserId externalUserId;

    String issuer;

    @ElementCollection
    Map<String, String> attributes = new HashMap<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ExternalUserId getExternalUserId() {
        return externalUserId;
    }

    public void setExternalUserId(ExternalUserId externalUserId) {
        this.externalUserId = externalUserId;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    protected Map<String, String> getAttributes() {
        return attributes;
    }

    protected void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }


    //Wrappers are implemented due to SAML specifications which are not implemented in the database.
    //Some changes to the database is needed in order fully support multi-valued attributes
    //These wrappers are hopefully temporary.
    //TODO: Refactor database to support multivalued attributes.
    @Transient
    public Map<String, List<String>> getAttributesList() {
        char recordSplitter = 0x1e;
        Map<String, String> attributes = this.getAttributes();
        Map<String, List<String>> newAttributes = new HashMap<>();
        for(Map.Entry<String, String> entry : attributes.entrySet()) {
            List<String> attributeValues = new ArrayList<>();
            Collections.addAll(attributeValues, entry.getValue().split(String.valueOf(recordSplitter)));
            newAttributes.put(entry.getKey(), attributeValues);
        }
        return newAttributes;
    }

    @Transient
    public void setAttributesList(Map<String, List<String>> _attributes) {
        char recordSplitter = 0x1e;
        Map<String, String> newAttribute = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : _attributes.entrySet()) {
            List<String> valueList = entry.getValue();
            String combined;
            combined = String.join(String.valueOf(recordSplitter), valueList);
            newAttribute.put(entry.getKey(), combined);
        }
        setAttributes(newAttribute);
    }
}
