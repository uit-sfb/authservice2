package no.uit.metapipe.auth.core;

import com.google.common.collect.ImmutableSet;
import no.uit.metapipe.auth.config.ClientCredentialsConfig;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

public abstract class Client {
    public Client() {
    }

    public static Client fromConfig(ClientCredentialsConfig config) {
        return new SystemClient(config);
    }

    public boolean canRedirectTo(URI uri) {
        for(URI other : getRedirectWhitelist()) {
            if(other.getScheme().equals(uri.getScheme()) &&
                    other.getHost().equals(uri.getHost()) &&
                    other.getPath().equals(uri.getPath())) {
                return true;
            }
        }
        return false;
    }

    public boolean canRedirectTo(String uri) {
        return canRedirectTo(URI.create(uri));
    }

    public boolean hasAuthorizedGrant(String authorizationGrant) {
        return getGrants().contains(authorizationGrant);
    }

    public abstract boolean isSystemClient();

    public abstract boolean isValidSecret(String secret);

    public abstract Set<Scope> getScopes();

    public abstract String getClientId();

    public abstract Set<String> getGrants();

    public abstract Set<URI> getRedirectWhitelist();
}

class SystemClient extends Client {
    ClientCredentialsConfig config;

    public SystemClient(ClientCredentialsConfig config) {
        this.config = config;
    }

    public boolean isSystemClient() {
        return true;
    }

    public boolean isValidSecret(String secret) {
        return config.compareSecret(secret);
    }

    public Set<Scope> getScopes() {
        Set<Scope> scopes = new HashSet<>();
        for(String scopeStr : config.getScopes()) {
            scopes.add(Scope.fromString(scopeStr));
        }
        return scopes;
    }

    @Override
    public String getClientId() {
        return config.getClientId();
    }

    @Override
    public Set<String> getGrants() {
        return ImmutableSet.copyOf(config.getGrants());
    }

    @Override
    public Set<URI> getRedirectWhitelist() {
        ImmutableSet.Builder<URI> uris = ImmutableSet.builder();
        for (String redirect : config.getRedirects()) {
            uris.add(URI.create(redirect));
        }
        return uris.build();
    }
}
