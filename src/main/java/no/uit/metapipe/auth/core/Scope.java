package no.uit.metapipe.auth.core;

import no.uit.metapipe.auth.utils.UriScope;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Scope")
public class Scope implements Cloneable {
    private long id;
    private Token token;
    private Set<String> methods = new HashSet<>();
    private String uri;

    public static Scope fromString(String encoded) {
        String[] parts = encoded.split("\\|");
        if (parts.length != 2) {
            throw new IllegalArgumentException("Not a valid scope");
        }
        String[] methods = parts[0].split(",");
        String uri = parts[1];
        Set<String> methodsSet = new HashSet<>();
        for(String m : methods) {
            methodsSet.add(m);
        }
        Scope scope = new Scope();
        scope.setMethods(methodsSet);
        scope.setUri(uri);
        return scope;
    }

    public static Scope fromUriScope(UriScope uriScope) {
        return fromString(uriScope.encodeString());
    }

    public String encodeString() {
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        for(String m : getMethods()) {
            if(!first) {
                builder.append(',');
            }
            first = false;
            builder.append(m);
        }
        builder.append('|');
        builder.append(getUri());
        return builder.toString();
    }

    public UriScope toUriScope() {
        return UriScope.fromString(encodeString());
    }

    public void addMethods(String... methods) {
        for(String m : methods) {
            getMethods().add(m);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name="token_id")
    public Token getToken() {
        return token;
    }
    public void setToken(Token token) {
        this.token = token;
    }

    @ElementCollection
    @Column(nullable = false)
    public Set<String> getMethods() {
        return methods;
    }
    public void setMethods(Set<String> methods) {
        this.methods = methods;
    }

    @Column(nullable = false)
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }
}
